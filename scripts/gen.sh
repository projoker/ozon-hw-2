#!/bin/bash

protoc --go_out=../pkg --go-grpc_out=../pkg --grpc-gateway_out=../pkg --go_opt=paths=source_relative --go-grpc_opt=paths=source_relative  --grpc-gateway_opt=paths=source_relative  --grpc-gateway_opt=generate_unbound_methods=true --proto_path=.. ../api/api.proto