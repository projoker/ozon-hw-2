# Бот Что-Где-Когда?

Бот, который позволяет запустить в чате викторину ЧГК.

## TODO

* Улучшить тесты

## Возможности

* Вопросы беруться из открытой базы https://db.chgk.info/
* Бот работает как в личном, так и групповом чате
* Взаимодействие с ботом происходит через команды
* Доступно создание/завершение игры, выдача вопроса, запоминание и проверка ответов
* Все результаты хранятся в бд, личная статистика может быть показана по запросу

## Команды

* /register - вступить в клуб ЧГК
* /stats - получить статистику
* /start - начать игру
* /end - завершить игру
* /question - получить вопрос
* /answer - ответить на вопрос
* /result - завершить вопрос

## Реализация

### Схема

```mermaid
flowchart LR
    subgraph Docker Compose App
        subgraph Telegram Service
            Client([Client])
            Poller
        end
        subgraph Chgkbot Service
            Proxy[Reverse proxy]
            Server((Server))
        end
        subgraph PostgreSQL Service
            DB[(Database)]
        end
        subgraph Migrations Service
            Migrations([Goose])
        end
    end
    subgraph External
        Telegram{{Telegram API}}
        Direct{{Direct Acess}}
        Chgk{{Chgk DB}}
    end

    Telegram<-->|long polling|Poller-->|go chan|Client-->|gRPC|Server
    Client-->|http|Telegram
    Chgk<-->|get question|Server
    Direct<-->|gRPC|Server
    Direct<-->|REST|Proxy<-->|gRPC|Server
    Migrations-->|migrations|DB
    Server<-->|SQL|DB
```

### Telegram Service

Выступает в роли UI. Запрашивает обновления у Telegram API по http при помощи long polling, обрабатывает
пользовательские команды и транслирует их серверу по gRPC

### Chgkbot Service

Занимается организацией игр ЧГК. Получает запросы по gRPC или REST (через обратный прокси). Запрашивает вопросы у базы
ЧГК по http и получает/отправляет статистику в БД при помощи SQL запросов

### Migrations Service

Применяет миграции к БД при помощи Goose

### PostgreSQL Service

Хранит информацию о членах клуба, играх, вопросах и ответах в PostgreSQL

## Установка

Запуск в docker-compose: `docker-compose -f deployments/docker-compose.yml up -d`