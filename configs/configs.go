package configs

import (
	"gopkg.in/yaml.v3"
	"log"
	"os"
)

type api struct {
	Url   string `yaml:"url"`
	Token string `yaml:"token"`
}

type Command struct {
	Name string `yaml:"name"`
	Desc string `yaml:"desc"`
}

type ClientConfig struct {
	Api     api `yaml:"api"`
	Timeout struct {
		Client int `yaml:"client"`
		Poller int `yaml:"poller"`
	} `yaml:"timeout"`
	Commands map[string]Command `yaml:"commands"`
}

type ServerConfig struct {
	Api        string `yaml:"api"`
	Timeout    int    `yaml:"timeout"`
	MaxRetries int    `yaml:"maxRetries"`
	DsnDB      string `yaml:"dsnDB"`
}

func ParseConfig(path string, config interface{}) error {
	confFile, err := os.ReadFile(path)
	if err != nil {
		log.Fatalf("failed to read configs: %v", err)
	}

	err = yaml.Unmarshal(confFile, config)

	return err
}
