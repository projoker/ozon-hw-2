package chgkbot

import (
	"context"
	"github.com/jackc/pgx/v4"
	"gitlab.ozon.dev/Projoker/homework-2/pkg/api"
	"google.golang.org/protobuf/types/known/emptypb"
	"log"
)

func (server *Server) StartGame(ctx context.Context, req *api.StartGameReq) (resp *emptypb.Empty, err error) {
	resp = &emptypb.Empty{}
	_, err = server.R.GetPlayer(ctx, req.CreatorId)
	if err == pgx.ErrNoRows {
		err = api.ErrorNotRegistered
		return
	}
	if err != nil {
		return
	}

	_, err = server.R.GetGame(ctx, req.ParentId)
	if err == nil {
		err = api.ErrorGameStarted
		return
	}
	if err != pgx.ErrNoRows {
		return
	}

	err = server.R.CreateGame(ctx, req.ParentId, req.CreatorId)
	if err != nil {
		return
	}

	log.Printf("New game! Parent: %v, creator: %v\n", req.ParentId, req.CreatorId)

	return
}

func (server *Server) EndGame(ctx context.Context, req *api.EndGameReq) (resp *emptypb.Empty, err error) {
	resp = &emptypb.Empty{}
	err = server.R.EndGame(ctx, req.ParentId)

	return
}
