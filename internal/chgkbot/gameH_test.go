package chgkbot

import (
	"context"
	"github.com/gojuno/minimock/v3"
	"github.com/jackc/pgx/v4"
	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/Projoker/homework-2/configs"
	"gitlab.ozon.dev/Projoker/homework-2/internal/models"
	"gitlab.ozon.dev/Projoker/homework-2/pkg/api"
	"google.golang.org/protobuf/types/known/emptypb"
	"testing"
)

func TestStartGame(t *testing.T) {
	mc := minimock.NewController(t)
	defer mc.Finish()

	mockRepo := NewRepositoryMock(mc)
	mockRepo.GetPlayerMock.Return(models.Player{"1", "1"}, nil)
	mockRepo.GetGameMock.Return(models.Game{}, pgx.ErrNoRows)
	mockRepo.CreateGameMock.Return(nil)
	svc := New(&configs.ServerConfig{}, mockRepo, nil)

	ctx := context.Background()
	resp, err := svc.StartGame(ctx, &api.StartGameReq{ParentId: "1", CreatorId: "1"})

	assert.Nil(t, err)
	assert.Equal(t, resp, &emptypb.Empty{})
}

func TestEndGame(t *testing.T) {
	mc := minimock.NewController(t)
	defer mc.Finish()

	mockRepo := NewRepositoryMock(mc)
	mockRepo.EndGameMock.Return(nil)
	svc := New(&configs.ServerConfig{}, mockRepo, nil)

	ctx := context.Background()
	resp, err := svc.EndGame(ctx, &api.EndGameReq{ParentId: "1"})

	assert.Nil(t, err)
	assert.Equal(t, resp, &emptypb.Empty{})
}
