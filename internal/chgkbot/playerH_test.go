package chgkbot

import (
	"context"
	"github.com/gojuno/minimock/v3"
	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/Projoker/homework-2/configs"
	"gitlab.ozon.dev/Projoker/homework-2/internal/models"
	"gitlab.ozon.dev/Projoker/homework-2/pkg/api"
	"google.golang.org/protobuf/types/known/emptypb"
	"testing"
)

func TestRegisterGame(t *testing.T) {
	mc := minimock.NewController(t)
	defer mc.Finish()

	mockRepo := NewRepositoryMock(mc)
	mockRepo.CreatePlayerMock.Return(nil)
	svc := New(&configs.ServerConfig{}, mockRepo, nil)

	ctx := context.Background()
	resp, err := svc.Register(ctx, &api.RegisterReq{PlayerId: "1", Name: "1"})

	assert.Nil(t, err)
	assert.Equal(t, resp, &emptypb.Empty{})
}

func TestStatsGame(t *testing.T) {
	mc := minimock.NewController(t)
	defer mc.Finish()

	mockRepo := NewRepositoryMock(mc)
	mockRepo.GetPlayerStatsMock.Return(models.PlayerStats{1, 1, 1}, nil)
	svc := New(&configs.ServerConfig{}, mockRepo, nil)

	ctx := context.Background()
	resp, err := svc.Stats(ctx, &api.StatsReq{PlayerId: "1"})

	assert.Nil(t, err)
	assert.Equal(t, resp, &api.StatsResp{Created: 1, Games: 1, Correct: 1})
}
