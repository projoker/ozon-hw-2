package chgkbot

import (
	"gitlab.ozon.dev/Projoker/homework-2/internal/models"
)

type ChgkDB interface {
	GetQuestion() (models.QuestionData, error)
}
