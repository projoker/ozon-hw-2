package chgkbot

import (
	"context"
	"gitlab.ozon.dev/Projoker/homework-2/internal/models"
)

type Repository interface {
	CreatePlayer(ctx context.Context, playerId, name string) (err error)
	GetPlayer(ctx context.Context, playerId string) (player models.Player, err error)
	GetPlayerStats(ctx context.Context, playerId string) (stats models.PlayerStats, err error)
	CreateGame(ctx context.Context, parentId string, creatorId string) (err error)
	GetGame(ctx context.Context, parentId string) (game models.Game, err error)
	EndGame(ctx context.Context, parentId string) (err error)
	CreateQuestion(ctx context.Context, question models.Question) (err error)
	GetQuestionOpen(ctx context.Context, gameId int64) (question models.Question, err error)
	EndQuestion(ctx context.Context, gameId int64) (question models.Question, err error)
	CreateAnswer(ctx context.Context, answer models.Answer) (err error)
	GetAnswers(ctx context.Context, gameId int64, questionId int64) (answers map[string]bool, err error)
}
