package chgkbot

import (
	"context"
	"github.com/jackc/pgx/v4"
	"gitlab.ozon.dev/Projoker/homework-2/internal/models"
	"gitlab.ozon.dev/Projoker/homework-2/pkg/api"
	"google.golang.org/protobuf/types/known/emptypb"
	"log"
	"regexp"
	"strings"
)

func (server *Server) StartQuestion(ctx context.Context, req *api.StartQuestionReq) (resp *api.StartQuestionResp, err error) {
	game, err := server.R.GetGame(ctx, req.ParentId)
	if err == pgx.ErrNoRows {
		err = api.ErrorGameNotStarted
		return
	}
	if err != nil {
		return
	}

	_, err = server.R.GetQuestionOpen(ctx, game.Id)
	if err == nil {
		err = api.ErrorQuestionStarted
		return
	}
	if err != pgx.ErrNoRows {
		return
	}

	question, err := server.ChgkDB.GetQuestion()
	if err != nil {
		return
	}

	err = server.R.CreateQuestion(ctx, models.Question{GameId: game.Id, Answer: question.Answer, Comments: question.Comments})
	if err != nil {
		return
	}

	log.Printf("New Question! Id: %v, answer: %v\n", question.QuestionId, question.Answer)

	resp = &api.StartQuestionResp{Question: question.Question}
	return
}

func (server *Server) AnswerQuestion(ctx context.Context, req *api.AnswerQuestionReq) (resp *emptypb.Empty, err error) {
	resp = &emptypb.Empty{}
	game, err := server.R.GetGame(ctx, req.ParentId)
	if err == pgx.ErrNoRows {
		err = api.ErrorGameNotStarted
		return
	}
	if err != nil {
		return
	}

	question, err := server.R.GetQuestionOpen(ctx, game.Id)
	if err == pgx.ErrNoRows {
		err = api.ErrorQuestionNotStarted
		return
	}
	if err != nil {
		return
	}

	err = server.R.CreateAnswer(ctx, models.Answer{
		game.Id,
		question.Id,
		req.PlayerId,
		isAnswer(question.Answer, req.Answer),
	})
	return
}

func (server *Server) EndQuestion(ctx context.Context, req *api.EndQuestionReq) (resp *api.EndQuestionResp, err error) {
	game, err := server.R.GetGame(ctx, req.ParentId)
	if err == pgx.ErrNoRows {
		err = api.ErrorGameNotStarted
		return
	}
	if err != nil {
		return
	}

	question, err := server.R.EndQuestion(ctx, game.Id)
	if err == pgx.ErrNoRows {
		err = api.ErrorQuestionNotStarted
		return
	}
	if err != nil {
		return
	}

	answers, err := server.R.GetAnswers(ctx, game.Id, question.Id)
	if err != nil {
		return
	}

	resp = &api.EndQuestionResp{Answer: question.Answer, Comments: question.Comments, Correct: answers}
	return
}

func isAnswer(text string, answer string) bool {
	text = strings.ToLower(text)
	answer = strings.ToLower(answer)

	if len([]rune(text)) > 3 {
		re := regexp.MustCompile("[^a-zA-Zа-яА-Я0-9]+")
		text = re.ReplaceAllString(text, "")
		answer = re.ReplaceAllString(answer, "")
	}

	return text == answer
}
