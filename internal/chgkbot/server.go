package chgkbot

import (
	"gitlab.ozon.dev/Projoker/homework-2/configs"
	"gitlab.ozon.dev/Projoker/homework-2/pkg/api"
)

type Server struct {
	Conf   *configs.ServerConfig
	R      Repository
	ChgkDB ChgkDB
	api.UnimplementedChgkServiceServer
}

func New(conf *configs.ServerConfig, r Repository, chgkDb ChgkDB) *Server {
	return &Server{Conf: conf, R: r, ChgkDB: chgkDb}
}
