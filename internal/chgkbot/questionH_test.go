package chgkbot

import (
	"context"
	"github.com/gojuno/minimock/v3"
	"github.com/jackc/pgtype"
	"github.com/jackc/pgx/v4"
	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/Projoker/homework-2/configs"
	"gitlab.ozon.dev/Projoker/homework-2/internal/models"
	"gitlab.ozon.dev/Projoker/homework-2/pkg/api"
	"google.golang.org/protobuf/types/known/emptypb"
	"testing"
)

func TestStartQuestion(t *testing.T) {
	mc := minimock.NewController(t)
	defer mc.Finish()

	mockRepo := NewRepositoryMock(mc)
	mockRepo.GetGameMock.Return(models.Game{1, "1", "1", pgtype.Timestamp{}, false}, nil)
	mockRepo.GetQuestionOpenMock.Return(models.Question{}, pgx.ErrNoRows)
	mockRepo.CreateQuestionMock.Return(nil)
	mockChgkDB := NewChgkDBMock(mc)
	mockChgkDB.GetQuestionMock.Return(models.QuestionData{1, "1", "1", "1"}, nil)
	svc := New(&configs.ServerConfig{}, mockRepo, mockChgkDB)

	ctx := context.Background()
	resp, err := svc.StartQuestion(ctx, &api.StartQuestionReq{ParentId: "1"})

	assert.Nil(t, err)
	assert.Equal(t, resp, &api.StartQuestionResp{Question: "1"})
}

func TestAnswerQuestion(t *testing.T) {
	mc := minimock.NewController(t)
	defer mc.Finish()

	mockRepo := NewRepositoryMock(mc)
	mockRepo.GetGameMock.Return(models.Game{1, "1", "1", pgtype.Timestamp{}, false}, nil)
	mockRepo.GetQuestionOpenMock.Return(models.Question{1, 1, "1", "1", false}, nil)
	mockRepo.CreateAnswerMock.Return(nil)
	svc := New(&configs.ServerConfig{}, mockRepo, nil)

	ctx := context.Background()
	resp, err := svc.AnswerQuestion(ctx, &api.AnswerQuestionReq{ParentId: "1", PlayerId: "1", Answer: "1"})

	assert.Nil(t, err)
	assert.Equal(t, resp, &emptypb.Empty{})
}

func TestEndQuestion(t *testing.T) {
	mc := minimock.NewController(t)
	defer mc.Finish()

	mockRepo := NewRepositoryMock(mc)
	mockRepo.GetGameMock.Return(models.Game{1, "1", "1", pgtype.Timestamp{}, false}, nil)
	mockRepo.EndQuestionMock.Return(models.Question{1, 1, "1", "1", false}, nil)
	mockRepo.GetAnswersMock.Return(map[string]bool{"1": true}, nil)
	svc := New(&configs.ServerConfig{}, mockRepo, nil)

	ctx := context.Background()
	resp, err := svc.EndQuestion(ctx, &api.EndQuestionReq{ParentId: "1"})

	assert.Nil(t, err)
	assert.Equal(t, resp, &api.EndQuestionResp{Answer: "1", Comments: "1", Correct: map[string]bool{"1": true}})
}
