package chgkbot

import (
	"context"
	"gitlab.ozon.dev/Projoker/homework-2/pkg/api"
	"google.golang.org/protobuf/types/known/emptypb"
	"log"
)

func (server *Server) Register(ctx context.Context, req *api.RegisterReq) (resp *emptypb.Empty, err error) {
	resp = &emptypb.Empty{}
	err = server.R.CreatePlayer(ctx, req.PlayerId, req.Name)
	if err != nil {
		return
	}

	log.Printf("New player! Id: %v, name: %v\n", req.PlayerId, req.Name)

	return
}

func (server *Server) Stats(ctx context.Context, req *api.StatsReq) (resp *api.StatsResp, err error) {
	stats, err := server.R.GetPlayerStats(ctx, req.PlayerId)
	if err != nil {
		return
	}

	resp = &api.StatsResp{Created: stats.Created, Games: stats.Games, Correct: stats.Correct}

	return
}
