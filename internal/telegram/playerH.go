package telegram

import (
	"context"
	"errors"
	"fmt"
	"gitlab.ozon.dev/Projoker/homework-2/pkg/api"
	"log"
	"strconv"
)

func (client *Client) OnRegister(ctx context.Context, chatId int, userId int, name string) {
	_, err := (*client.Cl).Register(ctx, &api.RegisterReq{PlayerId: strconv.Itoa(userId), Name: name})
	if err != nil {
		text := "Ошибка при вступлении в клуб ЧГК, попробуйте позднее"
		if errors.Is(err, api.ErrorRegistered) {
			text = "Вы уже член клуба ЧГК!"
		} else {
			log.Printf("failed to register: %v\n", err)
		}

		err = client.SendMessage(chatId, text)
		if err != nil {
			log.Printf("failed to send err: %v\n", err)
		}
		return
	}

	err = client.SendMessage(chatId, "Теперь вы член клуба ЧГК!")
	if err != nil {
		log.Printf("failed to send register: %v\n", err)
	}
}

func (client *Client) OnStats(ctx context.Context, chatId int, userId int) {
	resp, err := (*client.Cl).Stats(ctx, &api.StatsReq{PlayerId: strconv.Itoa(userId)})
	if err != nil {
		log.Printf("failed to get stats: %v\n", err)

		err = client.SendMessage(chatId, "Ошибка при получении статистики, попробуйте позднее")
		if err != nil {
			log.Printf("failed to send err: %v\n", err)
		}
		return
	}

	text := fmt.Sprintf("<b>Создано игр:</b> %v\n<b>Отвечено вопросов:</b> %v", resp.Created, resp.Games)
	if resp.Games > 0 {
		text += fmt.Sprintf("\n<b>Правильных ответов:</b> %v (%v%%)", resp.Correct, int(float64(resp.Correct)/float64(resp.Games)*100))
	}
	err = client.SendMessage(chatId, text)
	if err != nil {
		log.Printf("failed to send register: %v\n", err)
	}
}
