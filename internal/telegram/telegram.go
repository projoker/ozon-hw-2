package telegram

import (
	"bytes"
	"encoding/json"
	"errors"
	"html"
	"strings"
)

func (client *Client) SetCommands() error {
	var body Commands
	for _, v := range client.Commands {
		body.Commands = append(body.Commands, command{v.Name, v.Desc})
	}
	payload, err := json.Marshal(body)
	if err != nil {
		return err
	}

	_, err = client.http.Post(client.baseUrl+"setMyCommands", "application/json", bytes.NewBuffer(payload))

	return err
}

func (client *Client) ParseCommand(message Message) (string, string, error) {
	for _, entity := range message.Entities {
		if entity.Type == "bot_command" {
			text := []rune(message.Text)
			command := string(text[entity.Offset : entity.Offset+entity.Length])
			command = strings.Split(command, "@")[0]

			if client.isCommand(command) {
				data := string(text[entity.Offset+entity.Length:])
				data = strings.TrimSpace(data)
				return command, data, nil
			}
		}
	}

	return "", "", errors.New("wrong bot command")
}

func (client *Client) isCommand(text string) bool {
	for _, v := range client.Commands {
		if v.Name == text {
			return true
		}
	}

	return false
}

func (client *Client) SendMessage(chatId int, text string) error {
	body := messageBody{chatId, text, "HTML"}
	payload, err := json.Marshal(body)
	if err != nil {
		return err
	}

	_, err = client.http.Post(client.baseUrl+"sendMessage", "application/json", bytes.NewBuffer(payload))

	return err
}

func PrepareText(text string) string {
	text = strings.ReplaceAll(text, "\n", " ")
	html.EscapeString(text)

	return text
}
