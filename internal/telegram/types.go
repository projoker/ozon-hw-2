package telegram

import (
	"encoding/json"
)

type command struct {
	Command     string `json:"command"`
	Description string `json:"description"`
}

type Commands struct {
	Commands []command `json:"commands"`
}

type messageBody struct {
	ChatId    int    `json:"chat_id"`
	Text      string `json:"text"`
	ParseMode string `json:"parse_mode"`
}

type response struct {
	Ok          bool            `json:"ok"`
	Result      json.RawMessage `json:"result,omitempty"`
	ErrorCode   int             `json:"error_code,omitempty"`
	Description string          `json:"description,omitempty"`
}

type Update struct {
	UpdateId int     `json:"update_id"`
	Message  Message `json:"message,omitempty"`
}

type Message struct {
	From struct {
		Id       int    `json:"id"`
		Username string `json:"username,omitempty"`
	} `json:"from,omitempty"`

	Chat struct {
		Id int `json:"id"`
	} `json:"chat"`

	Entities []struct {
		Type   string `json:"type"`
		Offset int    `json:"offset"`
		Length int    `json:"length"`
	} `json:"entities,omitempty"`

	Text string `json:"text,omitempty"`
}
