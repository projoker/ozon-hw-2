package telegram

import (
	"context"
	"errors"
	"fmt"
	"gitlab.ozon.dev/Projoker/homework-2/pkg/api"
	"log"
	"strconv"
)

func (client *Client) OnStartQuestion(ctx context.Context, chatId int) {
	req := api.StartQuestionReq{ParentId: strconv.Itoa(chatId)}
	resp, err := (*client.Cl).StartQuestion(ctx, &req)
	if err != nil {
		text := "Ошибка при получении вопроса, попробуйте позднее"
		switch {
		case errors.Is(err, api.ErrorGameNotStarted):
			text = "Вам нужно начать игру!"
		case errors.Is(err, api.ErrorQuestionStarted):
			text = "У вас уже есть вопрос."
		default:
			log.Printf("failed to get question: %v\n", err)
		}

		err = client.SendMessage(chatId, text)
		if err != nil {
			log.Printf("failed to send err: %v\n", err)
		}
		return
	}

	question := fmt.Sprintf("<b>Новый вопрос!</b>\n\n%v", PrepareText(resp.Question))

	err = client.SendMessage(chatId, question)
	if err != nil {
		log.Printf("failed to send question: %v\n", err)
	}
}

func (client *Client) OnAnswerQuestion(ctx context.Context, chatId int, userId int, answer string) {
	req := api.AnswerQuestionReq{ParentId: strconv.Itoa(chatId), PlayerId: strconv.Itoa(userId), Answer: answer}
	_, err := (*client.Cl).AnswerQuestion(ctx, &req)
	if err != nil {
		text := "Ошибка при отправке ответа, попробуйте позднее."

		switch {
		case errors.Is(err, api.ErrorGameNotStarted):
			text = "Вам нужно начать игру!"
		case errors.Is(err, api.ErrorQuestionNotStarted):
			text = "У вас нет вопроса для ответа."
		case errors.Is(err, api.ErrorAnswerExists):
			text = "Вы уже давали ответ!"
		default:
			log.Printf("failed to give answer: %v\n", err)
		}

		err = client.SendMessage(chatId, text)
		if err != nil {
			log.Printf("failed to send err: %v\n", err)
		}
		return
	}

	err = client.SendMessage(chatId, "<b>Ответ засчитан!</b>")
	if err != nil {
		log.Printf("failed to send answer: %v\n", err)
	}
}

func (client *Client) OnEndQuestion(ctx context.Context, chatId int) {
	resp, err := (*client.Cl).EndQuestion(ctx, &api.EndQuestionReq{ParentId: strconv.Itoa(chatId)})
	if err != nil {
		text := "Ошибка при окончании вопроса, попробуйте позднее."
		if errors.Is(err, api.ErrorGameNotStarted) {
			text = "Вам нужно начать игру!"
		} else if errors.Is(err, api.ErrorQuestionNotStarted) {
			text = "У вас нет вопроса для окончания."
		} else {
			log.Printf("failed to verify question: %v\n", err)
		}

		err = client.SendMessage(chatId, text)
		if err != nil {
			log.Printf("failed to send err: %v\n", err)
		}
		return
	}

	text := fmt.Sprintf("<b>Ответ:</b> %v", PrepareText(resp.Answer))
	if len(resp.Comments) != 0 {
		text += fmt.Sprintf("\n\n<b>Комментарий:</b> %v", PrepareText(resp.Comments))
	}

	var correct, incorrect string
	for k, v := range resp.Correct {
		if v {
			correct += fmt.Sprintf(" @%v", k)
		} else {
			incorrect += fmt.Sprintf(" @%v", k)
		}
	}

	if len(correct) != 0 {
		text += fmt.Sprintf("\n\n<b>Угадали:</b>%v", correct)
	}

	if len(incorrect) != 0 {
		text += fmt.Sprintf("\n\n<b>Не угадали:</b>%v", incorrect)
	}

	err = client.SendMessage(chatId, text)
	if err != nil {
		log.Printf("failed to send verify: %v\n", err)
	}
}
