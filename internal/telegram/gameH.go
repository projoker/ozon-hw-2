package telegram

import (
	"context"
	"errors"
	"gitlab.ozon.dev/Projoker/homework-2/pkg/api"
	"log"
	"strconv"
)

func (client *Client) OnStartGame(ctx context.Context, chatId int, userId int) {
	req := api.StartGameReq{ParentId: strconv.Itoa(chatId), CreatorId: strconv.Itoa(userId)}
	_, err := (*client.Cl).StartGame(ctx, &req)
	if err != nil {
		text := "Ошибка при создании игры, попробуйте позднее"
		if errors.Is(err, api.ErrorGameStarted) {
			text = "Игра уже идёт!"
		} else if errors.Is(err, api.ErrorNotRegistered) {
			text = "Вы должны быть членом клуба ЧГК!"
		} else {
			log.Printf("failed to start game: %v\n", err)
		}

		err = client.SendMessage(chatId, text)
		if err != nil {
			log.Printf("failed to send err: %v\n", err)
		}
		return
	}

	err = client.SendMessage(chatId, "<b>Игра началась!</b>")
	if err != nil {
		log.Printf("failed to send game: %v\n", err)
	}
}

func (client *Client) OnEndGame(ctx context.Context, chatId int) {
	_, err := (*client.Cl).EndGame(ctx, &api.EndGameReq{ParentId: strconv.Itoa(chatId)})
	if err != nil {
		text := "Ошибка при окончании игры, попробуйте позднее"
		if errors.Is(err, api.ErrorGameNotStarted) {
			text = "Вам нужно начать игру!"
		} else {
			log.Printf("failed to end game: %v\n", err)
		}

		err = client.SendMessage(chatId, text)
		if err != nil {
			log.Printf("failed to send err: %v\n", err)
		}
		return
	}

	err = client.SendMessage(chatId, "<b>Игра окончена!</b>")
	if err != nil {
		log.Printf("failed to send finish: %v\n", err)
	}
}
