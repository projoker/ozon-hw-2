package telegram

import (
	"context"
	"gitlab.ozon.dev/Projoker/homework-2/configs"
	"gitlab.ozon.dev/Projoker/homework-2/pkg/api"
	"net/http"
	"time"
)

type Client struct {
	Cl       *api.ChgkServiceClient
	baseUrl  string
	Commands map[string]configs.Command
	http     *http.Client
	Poller   *LongPoller
	UpdateCh chan Update
}

func New(cl *api.ChgkServiceClient, api, token string, clientTimeout, pollerTimeout int, commands map[string]configs.Command) *Client {
	clientHttp := http.Client{Timeout: time.Duration(clientTimeout) * time.Second}
	client := Client{
		cl,
		api + token + "/",
		commands,
		&clientHttp,
		nil,
		make(chan Update),
	}

	pollerHttp := http.Client{Timeout: time.Duration(pollerTimeout*2) * time.Second}
	poller := LongPoller{
		&pollerHttp,
		client.baseUrl + "getUpdates",
		pollerTimeout,
		0,
		[]string{"\"message\""},
	}
	client.Poller = &poller

	return &client
}

func (client *Client) OnCommand(ctx context.Context, update Update) {
	command, data, err := client.ParseCommand(update.Message)
	if err != nil {
		return
	}

	switch command {
	case client.Commands["registerPlayer"].Name:
		client.OnRegister(ctx, update.Message.Chat.Id, update.Message.From.Id, update.Message.From.Username)
	case client.Commands["playerStats"].Name:
		client.OnStats(ctx, update.Message.Chat.Id, update.Message.From.Id)
	case client.Commands["startGame"].Name:
		client.OnStartGame(ctx, update.Message.Chat.Id, update.Message.From.Id)
	case client.Commands["endGame"].Name:
		client.OnEndGame(ctx, update.Message.Chat.Id)
	case client.Commands["startQuestion"].Name:
		client.OnStartQuestion(ctx, update.Message.Chat.Id)
	case client.Commands["answerQuestion"].Name:
		client.OnAnswerQuestion(ctx, update.Message.Chat.Id, update.Message.From.Id, data)
	case client.Commands["endQuestion"].Name:
		client.OnEndQuestion(ctx, update.Message.Chat.Id)
	}
}
