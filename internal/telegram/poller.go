package telegram

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
)

type LongPoller struct {
	http           *http.Client
	url            string
	timeout        int
	offset         int
	allowedUpdates []string
}

func (poller *LongPoller) Start(updateCh chan Update) {
	for {
		updates, err := poller.getUpdates()
		if err != nil {
			log.Println(err)
			continue
		}

		for _, update := range updates {
			poller.offset = update.UpdateId + 1
			updateCh <- update
		}
	}
}

func getRespResult(body []byte) ([]byte, error) {
	var resp response
	err := json.Unmarshal(body, &resp)
	if err != nil {
		return nil, err
	}

	if !resp.Ok {
		return nil, fmt.Errorf("%v: %v", resp.ErrorCode, resp.Description)
	}

	return resp.Result, nil
}

func (poller *LongPoller) getUpdates() ([]Update, error) {
	req, err := http.NewRequest("GET", poller.url, nil)
	if err != nil {
		return nil, err
	}

	q := req.URL.Query()
	q.Add("offset", strconv.Itoa(poller.offset))
	q.Add("timeout", strconv.Itoa(poller.timeout))
	q.Add("allowed_updates", fmt.Sprint(poller.allowedUpdates))
	req.URL.RawQuery = q.Encode()

	resp, err := poller.http.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	res, err := getRespResult(body)
	if err != nil {
		return nil, err
	}

	var updates []Update
	err = json.Unmarshal(res, &updates)
	if err != nil {
		return nil, err
	}

	return updates, nil
}
