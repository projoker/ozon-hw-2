package models

import "github.com/jackc/pgtype"

type Player struct {
	PlayerId string
	Name     string
}

type PlayerStats struct {
	Created int64
	Games   int64
	Correct int64
}

type Game struct {
	Id        int64
	ParentId  string
	CreatorId string
	CreatedAt pgtype.Timestamp
	Finished  bool
}

type Question struct {
	Id       int64
	GameId   int64
	Answer   string
	Comments string
	Finished bool
}

type QuestionData struct {
	QuestionId int
	Question   string
	Answer     string
	Comments   string
}

type Answer struct {
	GameId     int64
	QuestionId int64
	PlayerId   string
	Correct    bool
}
