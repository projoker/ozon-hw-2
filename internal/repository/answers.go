package repository

import (
	"context"
	"github.com/jackc/pgconn"
	"gitlab.ozon.dev/Projoker/homework-2/internal/models"
	"gitlab.ozon.dev/Projoker/homework-2/pkg/api"
)

func (r *repository) CreateAnswer(ctx context.Context, answer models.Answer) (err error) {
	const query = `
		INSERT INTO answers
		VALUES ($1, $2, $3, $4);
	`

	_, err = r.pool.Exec(ctx, query,
		answer.GameId,
		answer.QuestionId,
		answer.PlayerId,
		answer.Correct,
	)

	if pqErr, ok := err.(*pgconn.PgError); ok && pqErr.Code == "23505" { //unique_violation
		err = api.ErrorAnswerExists
	}

	return
}

func (r *repository) GetAnswers(ctx context.Context, gameId int64, questionId int64) (answers map[string]bool, err error) {
	const query = `
		SELECT name, correct
		FROM answers
		JOIN players p on answers.player_id = p.player_id
		WHERE game_id = $1
		  AND question_id = $2;
	`

	rows, err := r.pool.Query(ctx, query, gameId, questionId)
	if err != nil {
		return
	}
	defer rows.Close()

	answers = make(map[string]bool)

	for rows.Next() {
		var player string
		var correct bool

		err = rows.Scan(&player, &correct)
		if err != nil {
			return
		}

		answers[player] = correct
	}

	return
}
