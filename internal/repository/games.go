package repository

import (
	"context"
	"gitlab.ozon.dev/Projoker/homework-2/internal/models"
	"gitlab.ozon.dev/Projoker/homework-2/pkg/api"
)

func (r *repository) CreateGame(ctx context.Context, parentId string, creatorId string) (err error) {
	const query = `
		INSERT INTO games (parent_id, creator_id)
		VALUES ($1, $2)
	`

	_, err = r.pool.Exec(ctx, query,
		parentId,
		creatorId,
	)

	return
}

func (r *repository) GetGame(ctx context.Context, parentId string) (game models.Game, err error) {
	const query = `
		SELECT *
		FROM games
		WHERE parent_id = $1
		  AND finished = false;
	`

	err = r.pool.QueryRow(ctx, query, parentId).Scan(
		&game.Id,
		&game.ParentId,
		&game.CreatorId,
		&game.CreatedAt,
		&game.Finished,
	)

	return
}

func (r *repository) EndGame(ctx context.Context, parentId string) (err error) {
	const query = `
		UPDATE games
		SET finished = true
		WHERE parent_id = $1
		  AND finished = false;
	`

	cmd, err := r.pool.Exec(ctx, query, parentId)

	if cmd.RowsAffected() == 0 {
		err = api.ErrorGameNotStarted
		return
	}

	return
}
