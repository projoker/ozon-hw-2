package repository

import (
	"context"
	"github.com/jackc/pgconn"
	"gitlab.ozon.dev/Projoker/homework-2/internal/models"
	"gitlab.ozon.dev/Projoker/homework-2/pkg/api"
)

func (r *repository) CreatePlayer(ctx context.Context, playerId, name string) (err error) {
	const query = `
		INSERT INTO players
		VALUES ($1, $2);
	`

	_, err = r.pool.Exec(ctx, query, playerId, name)

	if pqErr, ok := err.(*pgconn.PgError); ok && pqErr.Code == "23505" { //unique_violation
		err = api.ErrorRegistered
	}

	return
}

func (r *repository) GetPlayer(ctx context.Context, playerId string) (player models.Player, err error) {
	const query = `
		SELECT *
		FROM players
		WHERE player_id = $1;
	`

	err = r.pool.QueryRow(ctx, query, playerId).Scan(
		&player.PlayerId,
		&player.Name,
	)

	return
}

func (r *repository) GetPlayerStats(ctx context.Context, playerId string) (stats models.PlayerStats, err error) {
	query := `
		SELECT *
		FROM (SELECT COUNT(*) AS created
			  FROM games
			  WHERE creator_id = $1) AS a,
			 (SELECT COUNT(*) as correct
			  FROM answers
			  WHERE player_id = $1
				AND correct = true) AS b,
			 (SELECT COUNT(*) as games
			  FROM answers
			  WHERE player_id = $1) AS c
		;
	`

	err = r.pool.QueryRow(ctx, query, playerId).Scan(
		&stats.Created,
		&stats.Correct,
		&stats.Games,
	)
	if err != nil {
		return
	}

	return
}
