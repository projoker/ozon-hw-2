package repository

import (
	"context"
	"gitlab.ozon.dev/Projoker/homework-2/internal/models"
)

func (r *repository) CreateQuestion(ctx context.Context, question models.Question) (err error) {
	const query = `
		INSERT INTO questions (game_id, answer, comments)
		VALUES ($1, $2, $3);
	`

	_, err = r.pool.Exec(ctx, query,
		question.GameId,
		question.Answer,
		question.Comments,
	)

	return
}

func (r *repository) GetQuestionOpen(ctx context.Context, gameId int64) (question models.Question, err error) {
	const query = `
		SELECT *
		FROM questions
		WHERE game_id = $1
		  AND finished = false;
	`

	err = r.pool.QueryRow(ctx, query, gameId).Scan(
		&question.Id,
		&question.GameId,
		&question.Answer,
		&question.Comments,
		&question.Finished,
	)

	return
}

func (r *repository) EndQuestion(ctx context.Context, gameId int64) (question models.Question, err error) {
	const query = `
		UPDATE questions
		SET finished = true
		WHERE game_id = $1
		  AND finished = false
		RETURNING *;
	`

	err = r.pool.QueryRow(ctx, query, gameId).Scan(
		&question.Id,
		&question.GameId,
		&question.Answer,
		&question.Comments,
		&question.Finished,
	)

	return
}
