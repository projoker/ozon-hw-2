package chgkDB

import (
	"encoding/xml"
)

type Search struct {
	XMLName  xml.Name `xml:"search"`
	Text     string   `xml:",chardata"`
	Question Question `xml:"question"`
}

type Question struct {
	Text                string `xml:",chardata"`
	TourFileName        string `xml:"tourFileName"`
	TournamentFileName  string `xml:"tournamentFileName"`
	QuestionId          int    `xml:"QuestionId"`
	ParentId            int    `xml:"ParentId"`
	Number              int    `xml:"Number"`
	Type                string `xml:"Type"`
	TypeNum             int    `xml:"TypeNum"`
	TextId              string `xml:"TextId"`
	Question            string `xml:"Question"`
	PassCriteria        string `xml:"PassCriteria"`
	Authors             string `xml:"Authors"`
	Sources             string `xml:"Sources"`
	Rating              string `xml:"Rating"`
	RatingNumber        string `xml:"RatingNumber"`
	Complexity          string `xml:"Complexity"`
	Topic               string `xml:"Topic"`
	ProcessedBySearch   string `xml:"ProcessedBySearch"`
	ParentTextID        string `xml:"parent_text_id"`
	ParentTextId        string `xml:"ParentTextId"`
	TourId              int    `xml:"tourId"`
	TournamentId        int    `xml:"tournamentId"`
	TourTitle           string `xml:"tourTitle"`
	TournamentTitle     string `xml:"tournamentTitle"`
	TourType            string `xml:"tourType"`
	TournamentType      string `xml:"tournamentType"`
	TourPlayedAt        string `xml:"tourPlayedAt"`
	TournamentPlayedAt  string `xml:"tournamentPlayedAt"`
	TourPlayedAt2       string `xml:"tourPlayedAt2"`
	TournamentPlayedAt2 string `xml:"tournamentPlayedAt2"`
	Notices             string `xml:"Notices"`
	Answer              string `xml:"Answer"`
	Comments            string `xml:"Comments"`
}
