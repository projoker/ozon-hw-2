package chgkDB

import (
	"encoding/xml"
	"gitlab.ozon.dev/Projoker/homework-2/internal/models"
	"gitlab.ozon.dev/Projoker/homework-2/pkg/api"
	"io"
	"net/http"
	"strings"
	"time"
)

type ChgkDB struct {
	url        string
	maxRetries int
}

func New(url string, maxRetries int) *ChgkDB {
	return &ChgkDB{url, maxRetries}
}

func (chgkDB *ChgkDB) GetQuestion() (models.QuestionData, error) {
	client := http.Client{Timeout: 5 * time.Second}
	for retries := 0; retries < chgkDB.maxRetries; retries++ {
		searchQ, err := searchQuestion(chgkDB.url, client)
		if err != nil {
			return models.QuestionData{}, err
		}

		if len(strings.Fields(searchQ.Answer)) == 1 {
			data := models.QuestionData{
				Question:   searchQ.Question,
				QuestionId: searchQ.QuestionId,
				Answer:     searchQ.Answer,
				Comments:   searchQ.Comments,
			}
			return data, nil
		}
	}

	return models.QuestionData{}, api.ErrorMaxRetries
}

func searchQuestion(url string, client http.Client) (Question, error) {
	resp, err := client.Get(url)
	if err != nil {
		return Question{}, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)

	search := Search{}
	err = xml.Unmarshal(body, &search)
	if err != nil {
		return Question{}, err
	}

	return search.Question, err
}
