package main

import (
	"context"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"gitlab.ozon.dev/Projoker/homework-2/configs"
	"gitlab.ozon.dev/Projoker/homework-2/internal/chgkDB"
	"gitlab.ozon.dev/Projoker/homework-2/internal/chgkbot"
	"gitlab.ozon.dev/Projoker/homework-2/internal/db"
	"gitlab.ozon.dev/Projoker/homework-2/internal/repository"
	"gitlab.ozon.dev/Projoker/homework-2/pkg/api"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	"net"
	"net/http"
)

func runProxy() {
	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())}
	err := api.RegisterChgkServiceHandlerFromEndpoint(ctx, mux, "localhost:5300", opts)
	if err != nil {
		log.Fatalf("failed to start rest: %v", err)
	}

	err = http.ListenAndServe(":8081", mux)
	if err != nil {
		log.Fatalf("failed to listen rest: %v", err)
	}
}

func main() {
	conf := configs.ServerConfig{}
	err := configs.ParseConfig("./configs/chgkbot.yaml", &conf)
	if err != nil {
		log.Fatalf("failed to parse configs: %v", err)
	}

	adp, err := db.New(context.Background(), conf.DsnDB)
	if err != nil {
		log.Fatalf("failed to connect DB: %v", err)
	}

	server := chgkbot.New(&conf, repository.New(adp), chgkDB.New(conf.Api, conf.MaxRetries))

	lis, err := net.Listen("tcp", ":5300")
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	go runProxy()

	grpcServer := grpc.NewServer()
	api.RegisterChgkServiceServer(grpcServer, server)
	err = grpcServer.Serve(lis)
	if err != nil {
		log.Fatalf("failed to start chgkbot: %v", err)
	}
}
