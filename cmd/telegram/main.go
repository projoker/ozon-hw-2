package main

import (
	"context"
	"gitlab.ozon.dev/Projoker/homework-2/configs"
	"gitlab.ozon.dev/Projoker/homework-2/internal/telegram"
	"gitlab.ozon.dev/Projoker/homework-2/pkg/api"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
)

func main() {
	conf := configs.ClientConfig{}
	err := configs.ParseConfig("./configs/telegram.yaml", &conf)
	if err != nil {
		log.Fatalf("failed to parse configs: %v", err)
	}

	conn, err := grpc.Dial("localhost:5300", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("failed to start client: %v", err)
	}
	defer conn.Close()

	cl := api.NewChgkServiceClient(conn)
	tg := telegram.New(&cl, conf.Api.Url, conf.Api.Token, conf.Timeout.Client, conf.Timeout.Poller, conf.Commands)

	err = tg.SetCommands()
	if err != nil {
		log.Fatalf("failed to set commands: %v", err)
	}

	go tg.Poller.Start(tg.UpdateCh)

	ctx := context.Background()
	for {
		update := <-tg.UpdateCh

		go tg.OnCommand(ctx, update)
	}
}
