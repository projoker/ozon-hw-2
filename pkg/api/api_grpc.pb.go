// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.20.1
// source: api/api.proto

package api

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// ChgkServiceClient is the client API for ChgkService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type ChgkServiceClient interface {
	Register(ctx context.Context, in *RegisterReq, opts ...grpc.CallOption) (*emptypb.Empty, error)
	Stats(ctx context.Context, in *StatsReq, opts ...grpc.CallOption) (*StatsResp, error)
	StartGame(ctx context.Context, in *StartGameReq, opts ...grpc.CallOption) (*emptypb.Empty, error)
	EndGame(ctx context.Context, in *EndGameReq, opts ...grpc.CallOption) (*emptypb.Empty, error)
	StartQuestion(ctx context.Context, in *StartQuestionReq, opts ...grpc.CallOption) (*StartQuestionResp, error)
	AnswerQuestion(ctx context.Context, in *AnswerQuestionReq, opts ...grpc.CallOption) (*emptypb.Empty, error)
	EndQuestion(ctx context.Context, in *EndQuestionReq, opts ...grpc.CallOption) (*EndQuestionResp, error)
}

type chgkServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewChgkServiceClient(cc grpc.ClientConnInterface) ChgkServiceClient {
	return &chgkServiceClient{cc}
}

func (c *chgkServiceClient) Register(ctx context.Context, in *RegisterReq, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/api.ChgkService/Register", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *chgkServiceClient) Stats(ctx context.Context, in *StatsReq, opts ...grpc.CallOption) (*StatsResp, error) {
	out := new(StatsResp)
	err := c.cc.Invoke(ctx, "/api.ChgkService/Stats", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *chgkServiceClient) StartGame(ctx context.Context, in *StartGameReq, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/api.ChgkService/StartGame", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *chgkServiceClient) EndGame(ctx context.Context, in *EndGameReq, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/api.ChgkService/EndGame", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *chgkServiceClient) StartQuestion(ctx context.Context, in *StartQuestionReq, opts ...grpc.CallOption) (*StartQuestionResp, error) {
	out := new(StartQuestionResp)
	err := c.cc.Invoke(ctx, "/api.ChgkService/StartQuestion", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *chgkServiceClient) AnswerQuestion(ctx context.Context, in *AnswerQuestionReq, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/api.ChgkService/AnswerQuestion", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *chgkServiceClient) EndQuestion(ctx context.Context, in *EndQuestionReq, opts ...grpc.CallOption) (*EndQuestionResp, error) {
	out := new(EndQuestionResp)
	err := c.cc.Invoke(ctx, "/api.ChgkService/EndQuestion", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ChgkServiceServer is the server API for ChgkService service.
// All implementations must embed UnimplementedChgkServiceServer
// for forward compatibility
type ChgkServiceServer interface {
	Register(context.Context, *RegisterReq) (*emptypb.Empty, error)
	Stats(context.Context, *StatsReq) (*StatsResp, error)
	StartGame(context.Context, *StartGameReq) (*emptypb.Empty, error)
	EndGame(context.Context, *EndGameReq) (*emptypb.Empty, error)
	StartQuestion(context.Context, *StartQuestionReq) (*StartQuestionResp, error)
	AnswerQuestion(context.Context, *AnswerQuestionReq) (*emptypb.Empty, error)
	EndQuestion(context.Context, *EndQuestionReq) (*EndQuestionResp, error)
	mustEmbedUnimplementedChgkServiceServer()
}

// UnimplementedChgkServiceServer must be embedded to have forward compatible implementations.
type UnimplementedChgkServiceServer struct {
}

func (UnimplementedChgkServiceServer) Register(context.Context, *RegisterReq) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Register not implemented")
}
func (UnimplementedChgkServiceServer) Stats(context.Context, *StatsReq) (*StatsResp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Stats not implemented")
}
func (UnimplementedChgkServiceServer) StartGame(context.Context, *StartGameReq) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method StartGame not implemented")
}
func (UnimplementedChgkServiceServer) EndGame(context.Context, *EndGameReq) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method EndGame not implemented")
}
func (UnimplementedChgkServiceServer) StartQuestion(context.Context, *StartQuestionReq) (*StartQuestionResp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method StartQuestion not implemented")
}
func (UnimplementedChgkServiceServer) AnswerQuestion(context.Context, *AnswerQuestionReq) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AnswerQuestion not implemented")
}
func (UnimplementedChgkServiceServer) EndQuestion(context.Context, *EndQuestionReq) (*EndQuestionResp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method EndQuestion not implemented")
}
func (UnimplementedChgkServiceServer) mustEmbedUnimplementedChgkServiceServer() {}

// UnsafeChgkServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to ChgkServiceServer will
// result in compilation errors.
type UnsafeChgkServiceServer interface {
	mustEmbedUnimplementedChgkServiceServer()
}

func RegisterChgkServiceServer(s grpc.ServiceRegistrar, srv ChgkServiceServer) {
	s.RegisterService(&ChgkService_ServiceDesc, srv)
}

func _ChgkService_Register_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(RegisterReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ChgkServiceServer).Register(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.ChgkService/Register",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ChgkServiceServer).Register(ctx, req.(*RegisterReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _ChgkService_Stats_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(StatsReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ChgkServiceServer).Stats(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.ChgkService/Stats",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ChgkServiceServer).Stats(ctx, req.(*StatsReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _ChgkService_StartGame_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(StartGameReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ChgkServiceServer).StartGame(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.ChgkService/StartGame",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ChgkServiceServer).StartGame(ctx, req.(*StartGameReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _ChgkService_EndGame_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(EndGameReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ChgkServiceServer).EndGame(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.ChgkService/EndGame",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ChgkServiceServer).EndGame(ctx, req.(*EndGameReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _ChgkService_StartQuestion_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(StartQuestionReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ChgkServiceServer).StartQuestion(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.ChgkService/StartQuestion",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ChgkServiceServer).StartQuestion(ctx, req.(*StartQuestionReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _ChgkService_AnswerQuestion_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AnswerQuestionReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ChgkServiceServer).AnswerQuestion(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.ChgkService/AnswerQuestion",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ChgkServiceServer).AnswerQuestion(ctx, req.(*AnswerQuestionReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _ChgkService_EndQuestion_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(EndQuestionReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ChgkServiceServer).EndQuestion(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.ChgkService/EndQuestion",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ChgkServiceServer).EndQuestion(ctx, req.(*EndQuestionReq))
	}
	return interceptor(ctx, in, info, handler)
}

// ChgkService_ServiceDesc is the grpc.ServiceDesc for ChgkService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var ChgkService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "api.ChgkService",
	HandlerType: (*ChgkServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Register",
			Handler:    _ChgkService_Register_Handler,
		},
		{
			MethodName: "Stats",
			Handler:    _ChgkService_Stats_Handler,
		},
		{
			MethodName: "StartGame",
			Handler:    _ChgkService_StartGame_Handler,
		},
		{
			MethodName: "EndGame",
			Handler:    _ChgkService_EndGame_Handler,
		},
		{
			MethodName: "StartQuestion",
			Handler:    _ChgkService_StartQuestion_Handler,
		},
		{
			MethodName: "AnswerQuestion",
			Handler:    _ChgkService_AnswerQuestion_Handler,
		},
		{
			MethodName: "EndQuestion",
			Handler:    _ChgkService_EndQuestion_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "api/api.proto",
}
