package api

import (
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var ErrorRegistered = status.Error(codes.AlreadyExists, "player already registered")
var ErrorNotRegistered = status.Error(codes.NotFound, "player not registered")
var ErrorGameStarted = status.Error(codes.AlreadyExists, "game already started")
var ErrorGameNotStarted = status.Error(codes.NotFound, "game not started")
var ErrorQuestionStarted = status.Error(codes.AlreadyExists, "question already started")
var ErrorQuestionNotStarted = status.Error(codes.NotFound, "question not started")
var ErrorAnswerExists = status.Error(codes.AlreadyExists, "answer already exists")
var ErrorMaxRetries = status.Error(codes.Aborted, "lot of retries for ok answer")
