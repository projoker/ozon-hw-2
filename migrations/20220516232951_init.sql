-- +goose Up
-- +goose StatementBegin
CREATE TABLE IF NOT EXISTS players
(
    player_id TEXT PRIMARY KEY,
    name TEXT
);
CREATE TABLE IF NOT EXISTS games
(
    id    SERIAL PRIMARY KEY,
    parent_id  TEXT    NOT NULL,
    creator_id TEXT REFERENCES players (player_id),
    created_at TIMESTAMP        DEFAULT now(),
    finished   BOOLEAN NOT NULL DEFAULT false
);
CREATE TABLE IF NOT EXISTS questions
(
    id SERIAL PRIMARY KEY,
    game_id     SERIAL REFERENCES games (id),
    answer      TEXT    NOT NULL,
    comments    TEXT    NOT NULL,
    finished    BOOLEAN NOT NULL DEFAULT false
);
CREATE TABLE IF NOT EXISTS answers
(
    game_id     SERIAL REFERENCES games (id),
    question_id INT REFERENCES questions (id),
    player_id   TEXT REFERENCES players (player_id),
    correct     BOOLEAN NOT NULL,
    UNIQUE (game_id, question_id, player_id)
);
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
DROP TABLE players CASCADE;
DROP TABLE games CASCADE;
DROP TABLE questions CASCADE;
DROP TABLE answers CASCADE;
-- +goose StatementEnd
